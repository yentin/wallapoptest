# WallapopTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.5.

## Development server

`npm run start` arranca el server local.  
Abrir el navegador y visitar `http://localhost:4200/` para acceder a la aplicación.

### Consideraciones del desarrollo
- Se ha creado un proxy de desarrollo para acceder al json de los datos y solventar errores de CORS. (ver proxy.conf.json)

- La aplicación muestra diferente número de items por fila dependiendo del ancho de pantalla

| Ancho   | Nº Items  |
| ------------ | ------------ |
| < 665 | 1  |
|  666 - 959 | 2  |
|  960 - 1279 | 3  |
|  1280 - 1599 | 4  |
|  >= 1600 | 5  |



## Running unit tests

`npm run test` ejecutará **ng test --code-coverage**. La cobertura se puede consultar en **coverage/wallapop-test/index.html**
