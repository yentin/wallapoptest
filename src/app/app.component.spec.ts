import { TestBed, async, ComponentFixture, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { BrowserModule } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DebugElement } from '@angular/core';
import { RouterModule, Routes, Router, RouterEvent } from '@angular/router';
import { SharedModule } from './shared/shared.module';



const translations: any = { TEST: 'This is a test' };

class FakeLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return of(translations);
  }
}

export const appRoutes: Routes = [
  {path: '', component: AppComponent}
  ];

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let app: DebugElement;
  let translateService: TranslateService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  const oldResetTestingModule = TestBed.resetTestingModule;

  beforeAll((done) => (async () => {
    TestBed.resetTestingModule();
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: FakeLoader
          }
        }),
        RouterModule.forRoot(appRoutes),
        RouterTestingModule,
        BrowserModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
      ]
    });

    await TestBed.compileComponents();

    // prevent Angular from resetting testing module
    TestBed.resetTestingModule = () => TestBed;

  })().then(done).catch(done.fail));

  beforeEach(async(() => {
      // Inject the http service and test controller for each test
      httpClient = TestBed.get(HttpClient);
      httpTestingController = TestBed.get(HttpTestingController);

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    app = fixture.debugElement;
    translateService = TestBed.get(TranslateService);
  });


  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it('should ngOnInit', () => {
    component.ngOnInit();
    expect(app).toBeTruthy();
    expect(component.defaultLanguage).toBe('es');
  });

  afterAll( () => {
    localStorage.clear();
    sessionStorage.clear();
    // reinstate resetTestingModule method
    TestBed.resetTestingModule = oldResetTestingModule;
    TestBed.resetTestingModule();
  });
});
