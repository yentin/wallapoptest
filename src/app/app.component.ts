import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'wallapop-test';
  defaultLanguage: string;

constructor(private translateService: TranslateService,
            private router: Router) {}

  ngOnInit() {
    this.initLanguage();
  }

  initLanguage() {
    this.defaultLanguage = 'es';
    this.translateService.addLangs(['es']);
    this.translateService.setDefaultLang(this.defaultLanguage);
    this.translateService.use(this.defaultLanguage);
  }
}
