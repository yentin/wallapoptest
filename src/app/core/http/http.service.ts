import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Injectable()
export class HttpService {

  constructor(private httpClient: HttpClient,
              private translateService: TranslateService) { }

  private addHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.append('Accept-Language', this.translateService.currentLang);
    headers = headers.append('Cache-Control', 'no-cache');
    headers = headers.append('Pragma', 'no-cache');
    headers = headers.append('X-Frame-Options', 'DENY');

    return headers;
  }

  httpGet(url: string): Observable<any> {
    const headers = this.addHeaders();
    return this.httpClient.get(url, { headers });
  }
}
