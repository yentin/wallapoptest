export class ProductCardObject {
    title: string;
    description: string;
    price: number;
    email: string;
    image: string;
    isFavourite: boolean;

    constructor(title: string, description: string, price: number, email: string, image: string) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.email = email;
        this.image = image;
        this.isFavourite = false;
    }
}
