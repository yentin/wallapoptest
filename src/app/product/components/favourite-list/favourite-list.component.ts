import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductCardObject } from '../../models/product-card-object.class';


@Component({
  selector: 'favourite-list',
  templateUrl: './favourite-list.component.html',
  styleUrls: ['./favourite-list.component.scss']
})
export class FavouriteListComponent implements OnInit {

@Input() productList: Array<ProductCardObject> = [];
@Output() deleteFavourite: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  addRemoveFavourite(item: ProductCardObject) {
    this.deleteFavourite.emit(item);
  }
}
