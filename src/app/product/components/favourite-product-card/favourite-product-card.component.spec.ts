
import { HttpClient } from '@angular/common/http';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { FavouriteProductCardComponent } from './favourite-product-card.component';
import { ProductCardObject } from '../../models/product-card-object.class';

const translations: any = { TEST: 'This is a test' };

class FakeLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return of(translations);
  }
}

describe('FavouriteProductCardComponent', () => {
  let component: FavouriteProductCardComponent;
  let fixture: ComponentFixture<FavouriteProductCardComponent>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  const oldResetTestingModule = TestBed.resetTestingModule;

  beforeAll((done) => (async () => {
    TestBed.resetTestingModule();
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: FakeLoader
          }
        })
      ],
      declarations: [
        FavouriteProductCardComponent
      ],
      providers: [
      ]
    });

    await TestBed.compileComponents();

    // prevent Angular from resetting testing module
    TestBed.resetTestingModule = () => TestBed;

  })().then(done).catch(done.fail));

  beforeEach(() => {
    // Inject the http service and test controller for each test
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouriteProductCardComponent);
    component = fixture.componentInstance;
    component.item = new ProductCardObject('title', 'description', 12, 'email', 'image');
    component.item.isFavourite = true;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should mark as favourite', () => {
    const emitter = spyOn(component.addRemoveFavourite, 'emit');
    component.favourite(true);
    expect(component.item.isFavourite).toBeTruthy();
    expect(emitter).toHaveBeenCalled();
  });

  afterAll( () => {
    localStorage.clear();
    sessionStorage.clear();
    // reinstate resetTestingModule method
    TestBed.resetTestingModule = oldResetTestingModule;
    TestBed.resetTestingModule();
  });
});
