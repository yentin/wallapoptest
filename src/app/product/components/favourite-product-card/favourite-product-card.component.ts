import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductCardObject } from '../../models/product-card-object.class';

@Component({
  selector: 'favourite-product-card',
  templateUrl: './favourite-product-card.component.html',
  styleUrls: ['./favourite-product-card.component.scss']
})
export class FavouriteProductCardComponent implements OnInit {
  @Input() item: ProductCardObject;
  @Input() itemsPerRow: number;  // Assign number of items per row to know when we need to change icons sizes
  @Output() addRemoveFavourite: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  favourite(isFavourite: boolean) {
    this.item.isFavourite = isFavourite;
    this.addRemoveFavourite.emit(this.item);
  }
}
