import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'skeleton-loading-list',
  templateUrl: './skeleton-loading-list.component.html',
  styleUrls: ['./skeleton-loading-list.component.scss']
})
export class SkeletonLoadingListComponent implements OnInit {
@Input() numItems: string = '5';
  items: Array<any> = [];

  constructor() { }

  ngOnInit() {
    const numItemsInteger = parseInt(this.numItems, 10);
    for (let i = 0; i < numItemsInteger; i++) {
      this.items[i] = i;
    }
  }
}
