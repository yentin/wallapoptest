import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkeletonLoadingListComponent } from './skeleton-loading-list.component';

describe('SkeletonLoadingListComponent', () => {
  let component: SkeletonLoadingListComponent;
  let fixture: ComponentFixture<SkeletonLoadingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkeletonLoadingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkeletonLoadingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.numItems = '4';
    expect(component).toBeTruthy();
  });
});
