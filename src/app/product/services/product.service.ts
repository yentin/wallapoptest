import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/core/http/http.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class ProductService {

  constructor(private httpService: HttpService) { }

  getProductList(): Observable<any> {
    const URL = `${environment.BASE_URL}/api`;
    return this.httpService.httpGet(URL);
  }
}
