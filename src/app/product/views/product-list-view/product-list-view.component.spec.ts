
import { HttpClient } from '@angular/common/http';
import { TestBed, ComponentFixture, inject } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { ProductListViewComponent } from './product-list-view.component';
import { ProductService } from '../../services/product.service';
import { HttpService } from 'src/app/core/http/http.service';
import { ProductCardComponent } from '../../components/product-card/product-card.component';
import { SkeletonLoadingListComponent } from '../../components/skeleton-loading-list/skeleton-loading-list.component';
import { ActivatedRoute } from '@angular/router';
import { FavouriteListComponent } from '../../components/favourite-list/favourite-list.component';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { ModalHeaderComponent } from 'src/app/shared/components/modal/modal-header/modal-header.component';
import { ModalFooterComponent } from 'src/app/shared/components/modal/modal-footer/modal-footer.component';
import { ModalBodyComponent } from 'src/app/shared/components/modal/modal-body/modal-body.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SearchComponent } from 'src/app/shared/components/search/search.component';
import { CollapsibleComponent } from 'src/app/shared/components/collapsible/collapsible.component';
import { ViewMoreItemsComponent } from 'src/app/shared/components/view-more-items/view-more-items.component';
import { FavouriteProductCardComponent } from '../../components/favourite-product-card/favourite-product-card.component';
import { TagKeywordResultComponent } from 'src/app/shared/components/tag-keyword-result/tag-keyword-result.component';
import { FormsModule } from '@angular/forms';
import { ProductCardObject } from '../../models/product-card-object.class';

const translations: any = { TEST: 'This is a test' };

class FakeLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return of(translations);
  }
}

describe('ProductListViewComponent', () => {
  let component: ProductListViewComponent;
  let fixture: ComponentFixture<ProductListViewComponent>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  const oldResetTestingModule = TestBed.resetTestingModule;

  beforeAll((done) => (async () => {
    TestBed.resetTestingModule();
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        FormsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: FakeLoader
          }
        })
      ],
      declarations: [
        ProductListViewComponent,
        ProductCardComponent,
        SkeletonLoadingListComponent,
        FavouriteListComponent,
        FavouriteProductCardComponent,
        ModalComponent,
        ModalHeaderComponent,
        ModalFooterComponent,
        ModalBodyComponent,
        SearchComponent,
        CollapsibleComponent,
        ViewMoreItemsComponent,
        TagKeywordResultComponent
      ],
      providers: [
        ProductService,
        HttpService
      ]
    });

    await TestBed.compileComponents();

    // prevent Angular from resetting testing module
    TestBed.resetTestingModule = () => TestBed;

  })().then(done).catch(done.fail));

  beforeEach(() => {
    // Inject the http service and test controller for each test
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });


  it('should add and remove a favourite', () => {
    component.favouriteItemsArray = [];
    const mockProduct = new ProductCardObject('title', 'description', 12, 'email', 'image');
    mockProduct.isFavourite = true;
    component.addRemoveFavourite(mockProduct);
    expect(component.favouriteItemsArray.length).toEqual(1);
    component.favouriteItemsArray[0].isFavourite = false;
    component.addRemoveFavourite(component.favouriteItemsArray[0]);
    expect(component.favouriteItemsArray.length).toEqual(0);
  });

  it('should search a product and clear', () => {
    const mockProduct = new ProductCardObject('title', 'description', 12, 'email', 'image');
    const mockProduct2 = new ProductCardObject('Bolso', 'descriptionB', 44, 'email', 'image');
    component.itemsArray.push(mockProduct);
    component.itemsArray.push(mockProduct2);
    component.filteredItemsArray = [... component.itemsArray];
    const filteredItemsLength = component.filteredItemsArray.length;
    component.searchProducts('Bolso');
    expect(component.filteredItemsArray.length).not.toEqual(filteredItemsLength);
    component.searchProducts('');
    expect(component.filteredItemsArray.length).toEqual(filteredItemsLength);
  });

  it('should search a favourite product and clear', () => {
    const mockProduct = new ProductCardObject('title', 'description', 12, 'email', 'image');
    const mockProduct2 = new ProductCardObject('Bolso', 'descriptionB', 44, 'email', 'image');
    component.favouriteItemsArray.push(mockProduct);
    component.favouriteItemsArray.push(mockProduct2);
    component.filteredFavouriteItemsArray = [... component.favouriteItemsArray];
    const filteredFavouriteItemsLength = component.filteredFavouriteItemsArray.length;
    component.searchFavouriteProducts('Bolso');
    expect(component.filteredFavouriteItemsArray.length).not.toEqual(filteredFavouriteItemsLength);
    component.searchFavouriteProducts('');
    expect(component.filteredFavouriteItemsArray.length).toEqual(filteredFavouriteItemsLength);
  });

  it('should open modal', () => {
    component.openModal();
    expect(component.showModal).toBeTruthy();
  });

  it('should close modal', () => {
    component.onCloseModal({});
    expect(component.showModal).toBeFalsy();
  });

  it('should order', () => {
    const mockProduct = new ProductCardObject('title', 'description2', 12, 'email', 'image');
    const mockProduct2 = new ProductCardObject('Bolso', 'description', 44, 'email', 'image');
    component.filteredItemsArray.push(mockProduct);
    component.filteredItemsArray.push(mockProduct2);
    let mockEvent = {id: 'orderTitle|asc'};
    component.onOrderItemsBy(mockEvent);
    expect(component.filteredItemsArray[0].title).toEqual('Bolso');
    mockEvent = {id: 'orderPrice|asc'};
    component.onOrderItemsBy(mockEvent);
    expect(component.filteredItemsArray[0].title).toEqual('title');
    mockEvent = {id: 'orderPrice|desc'};
    component.onOrderItemsBy(mockEvent);
    expect(component.filteredItemsArray[0].title).toEqual('Bolso');
    mockEvent = {id: 'orderEmail|asc'};
    component.onOrderItemsBy(mockEvent);
    expect(component.filteredItemsArray[0].title).toEqual('Bolso');
    mockEvent = {id: 'orderDescription|asc'};
    component.onOrderItemsBy(mockEvent);
    expect(component.filteredItemsArray[0].title).toEqual('Bolso');
    mockEvent = {id: ''};
    component.onOrderItemsBy(mockEvent);
    expect(component.filteredItemsArray[0].title).toEqual('Bolso');
  });


  it('should trigger recalc items per row', () => {
    const spyOnResize = spyOn(component, 'reCalcItemsPerRowEvent');
    window.dispatchEvent(new Event('resize'));
    expect(spyOnResize).toHaveBeenCalled();
  });

  it('should initProductList', (inject([ProductService], (productService: ProductService) => {
    spyOn(productService, 'getProductList').and.returnValue(
      of({
        items: [
          {
            title: 'iPhone 6S Oro',
            description: 'Vendo un iPhone 6 S color Oro nuevo y sin estrenar.',
            price: '740',
            email: 'iphonemail@wallapop.com',
            image: 'https://frontend-tech-test-data.s3-eu-west-1.amazonaws.com/img/iphone.png'
          },
          {
            title: 'Polaroid 635',
            description: 'Cámara clásica de fotos Polaroid, modelo 635. Las fotos son a super color.',
            price: '50',
            email: 'cameramail@wallapop.com',
            image: 'https://frontend-tech-test-data.s3-eu-west-1.amazonaws.com/img/camera.png'
          }
        ]}));
    component.initProductList();
    expect(component.itemsArray.length).toEqual(2);
    expect(component.itemsArray).toEqual(component.filteredItemsArray);
  })));


  afterAll( () => {
    localStorage.clear();
    sessionStorage.clear();
    // reinstate resetTestingModule method
    TestBed.resetTestingModule = oldResetTestingModule;
    TestBed.resetTestingModule();
  });
});
