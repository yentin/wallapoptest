import { Component, OnInit, HostListener } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { ProductCardObject } from '../../models/product-card-object.class';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'product-list-view',
  templateUrl: './product-list-view.component.html',
  styleUrls: ['./product-list-view.component.scss']
})
export class ProductListViewComponent implements OnInit {

  itemsPerRow: number;
  numItemsShow: number;
  showLoading: boolean;
  showModal: boolean;

  itemsArray: Array <ProductCardObject> = [];
  filteredItemsArray: Array <ProductCardObject> = [];
  favouriteItemsArray: Array <ProductCardObject> = [];
  filteredFavouriteItemsArray: Array <ProductCardObject> = [];
  collapsibleItems: Array<any>;

  selectedCollapsibleOption: any;
  routeQueryParams$: any;

  constructor(private productService: ProductService,
              private translateService: TranslateService) { }

  ngOnInit() {
    this.initBooleans();
    this.initProductList();
    this.initCollapsible();
    this.initValues();
  }

  private initBooleans() {
    this.showLoading = true;
  }
  private initCollapsible() {
    this.translateService.get('COLLAPSIBLE').subscribe(translate => {
      this.collapsibleItems = [
        {
          id: 'orderTitle|asc',
          title: (translate.ASCENDINGTITLE) ? translate.ASCENDINGTITLE : 'Título ascencente'
        },
        {
          id: 'orderTitle|desc',
          title: (translate.DESCENDINGTITLE) ? translate.DESCENDINGTITLE : 'Título descencente'
        },
        {
          id: 'orderPrice|asc',
          title: (translate.ASCENDINGPRICE) ? translate.ASCENDINGPRICE : 'Precio ascencente'
        },
        {
          id: 'orderPrice|desc',
          title: (translate.DESCENDINGPRICE) ? translate.DESCENDINGPRICE : 'Precio descencente'
        },
        {
          id: 'orderEmail|asc',
          title: (translate.ASCENDINGEMAIL) ? translate.ASCENDINGEMAIL : 'Email ascencente'
        },
        {
          id: 'orderEmail|desc',
          title: (translate.DESCENDINGEMAIL) ? translate.DESCENDINGEMAIL : 'Email descencente'
        },
        {
          id: 'orderDescription|asc',
          title: (translate.ASCENDINGDESCRIPTION) ? translate.ASCENDINGDESCRIPTION : 'Descripción ascencente'
        },
        {
          id: 'orderDescrition|desc',
          title: (translate.DESCENDINGDESCRIPTION) ? translate.DESCENDINGDESCRIPTION : 'Descripción descencente'
        }
      ];
      this.selectedCollapsibleOption = this.collapsibleItems[0];
    });
  }

  initProductList() {
    this.productService.getProductList().subscribe(data => {
      const itemObjectsArray: Array<any> = (data) ? data.items : [];
      itemObjectsArray.forEach ( value => {
        this.itemsArray.push(new ProductCardObject(value.title, value.description, value.price, value.email, value.image));
       });
      this.showLoading = false;
      this.filteredItemsArray = [... this.itemsArray];
      this.onOrderItemsBy({id: 'orderTitle|asc'});
    });
  }

  private initItemsPerRow() {
    if (window.innerWidth < 666) {
      this.itemsPerRow = 1;
    } else if (window.innerWidth >= 666 && window.innerWidth < 960) {
      this.itemsPerRow = 2;
    } else if (window.innerWidth >= 960 && window.innerWidth < 1280) {
      this.itemsPerRow = 3;
    } else if (window.innerWidth >= 1280 && window.innerWidth < 1600) {
      this.itemsPerRow = 4;
    } else if (window.innerWidth >= 1600) {
      this.itemsPerRow = 5;
    }
    this.numItemsShow = (this.numItemsShow < this.itemsPerRow) ? this.itemsPerRow : this.numItemsShow ;
  }

  private initValues() {
        this.initItemsPerRow();
        this.initNumItemsShow();
  }

  addRemoveFavourite(item) {
    let index;
    if (this.favouriteItemsArray) {
      index = this.favouriteItemsArray.indexOf(item, 0);
    }
    (item.isFavourite) ? this.favouriteItemsArray.push(item) : this.favouriteItemsArray.splice(index, 1);
    this.filteredFavouriteItemsArray = [... this.favouriteItemsArray];
  }

  searchProducts(searchKeyword) {
    this.filteredItemsArray = this.itemsArray.filter ( item => {
      return item.title.toLowerCase().includes(searchKeyword.toLowerCase()) ||
        item.description.toLowerCase().includes(searchKeyword.toLowerCase());
    });
    (searchKeyword === '') ? this.initNumItemsShow() : this.nextPage();
  }

  searchFavouriteProducts(searchKeyword) {
    this.filteredFavouriteItemsArray = this.favouriteItemsArray.filter ( item => {
      return item.title.toLowerCase().includes(searchKeyword.toLowerCase());
    });
  }

  initNumItemsShow() {
    this.numItemsShow = this.itemsPerRow;
  }

  nextPage() {
    this.numItemsShow += this.itemsPerRow;
    if ( this.numItemsShow > this.filteredItemsArray.length) {
      this.numItemsShow = this.filteredItemsArray.length;
    }
  }

  openModal() {
    this.showModal = true;
  }

  onCloseModal(event) {
    this.showModal = false;
  }

  onOrderItemsBy(event) {
    const sortType = event.id.split('|');
    switch (sortType[0]) {
      case 'orderTitle':
        this.orderByString('title');
        break;
      case 'orderPrice':
        this.orderByNumber('price');
        break;
      case 'orderEmail':
        this.orderByString('email');
        break;
      case 'orderDescription':
        this.orderByString('description');
        break;
      default:
        this.orderByString('title');
        break;
    }
    if (sortType[1] === 'desc') {
      this.filteredItemsArray.reverse();
    }
  }

  private orderByString(sortAttribute: string) {
    this.filteredItemsArray.sort((product1, product2) => {
      if (product1[sortAttribute].toLowerCase() < product2[sortAttribute].toLowerCase()) {return -1; }
      if (product1[sortAttribute].toLowerCase() > product2[sortAttribute].toLowerCase()) {return 1; }
      return 0;
    });
  }

  private orderByNumber(sortAttribute: string) {
    this.filteredItemsArray.sort((product1, product2) =>
      product1[sortAttribute] - product2[sortAttribute]
    );
  }

  @HostListener('window:resize', ['$event'])
  reCalcItemsPerRowEvent(event) {
    this.initItemsPerRow();
  }
}
