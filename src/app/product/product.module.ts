import { NgModule, LOCALE_ID } from '@angular/core';
import es from '@angular/common/locales/es';
import { CommonModule } from '@angular/common';
import { ProductListViewComponent } from './views/product-list-view/product-list-view.component';
import { ProductRoutingModule } from './product-routing.module';
import { ProductService } from './services/product.service';
import { SkeletonLoadingListComponent } from './components/skeleton-loading-list/skeleton-loading-list.component';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { ProductCardComponent } from './components/product-card/product-card.component';

import { registerLocaleData } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { SearchComponent } from '../shared/components/search/search.component';
import { FavouriteListComponent } from './components/favourite-list/favourite-list.component';
import { FavouriteProductCardComponent } from './components/favourite-product-card/favourite-product-card.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { environment } from 'src/environments/environment';

registerLocaleData(es);

@NgModule({
  declarations: [
    ProductListViewComponent,
    SkeletonLoadingListComponent,
    ProductCardComponent,
    FavouriteListComponent,
    FavouriteProductCardComponent  ],
  imports: [
    CommonModule,
    SharedModule,
    ProductRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http, './assets/i18n/', `.json?ver=${environment.version}`);
        },
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    ProductService,
    { provide: LOCALE_ID, useValue: 'es-ES' }
  ]
})
export class ProductModule { }
