import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { ModalComponent } from './modal.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { Observable ,  of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';

const translations: any = { 'TEST': 'This is a test' };

class FakeLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return of(translations);
  }
}

describe('ModalComponent(templateUrl)', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;
  const oldResetTestingModule = TestBed.resetTestingModule;

  beforeAll((done) => (async () => {
    TestBed.resetTestingModule();
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: FakeLoader
          }
        })
      ],
      declarations: [
        ModalComponent
      ],
      providers: [
      ]
    });

    await TestBed.compileComponents();

    // prevent Angular from resetting testing module
    TestBed.resetTestingModule = () => TestBed;

  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should has function close()', fakeAsync(() => {
    expect(component.close());
    tick(500);
  }));

  it('isClassVisible should be false', () => {
    expect(component.isClassVisible).toBeFalsy();
  });

  it('close() should change isClassVisible to true', () => {
    component.close();
    expect(component.isClassVisible).toBeTruthy();
  });

  it('unbindDocumentClickListener', () => {
    component.unbindDocumentClickListener();
    expect(true).toBeTruthy();
  });

  it('close() should change visible to false', fakeAsync(() => {
    component.visible = true;
    component.close();
    setTimeout(() => {
      expect(component.visible).toBeFalsy();
    }, 501);

    tick(501);

  }));

  afterAll( () => {
    localStorage.clear();
    sessionStorage.clear();
    // reinstate resetTestingModule method
    TestBed.resetTestingModule = oldResetTestingModule;
    TestBed.resetTestingModule();
  });

});
