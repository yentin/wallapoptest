import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, OnDestroy, ElementRef, Renderer2 } from '@angular/core';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, AfterViewInit {

  @Input() closable = true;
  @Input() visible = true;
  @Input() scroll: boolean;
  @Input() nocloseble: string;
  @Input() class: string;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onClose: EventEmitter<any> = new EventEmitter<any>();

  private timer: any;
  private documentClickListener: any;
  isClassVisible = false;

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private element: ElementRef) {

  }

  ngOnInit() {
    this.bindDocumentClickListener();
  }

  ngAfterViewInit(): void {
    const modalButtons = this.elementRef.nativeElement.querySelectorAll('button');
    for (const button of modalButtons) {
      if (!button.hasAttribute('noClose')) {
        button.addEventListener('click', () => {
          this.close();
        });
      }
    }
  }

  close() {
    if (this.nocloseble !== '') {
      this.isClassVisible = true;
      clearTimeout(this.timer);
      this.timer = setTimeout(() => {
        this.visible = false;
        this.visibleChange.emit(this.visible);
        this.isClassVisible = false;
        this.sendData();
      }, 500);
    }
  }

  sendData() {
    this.onClose.emit(false);
  }

  bindDocumentClickListener() {
    if (!this.documentClickListener) {
      this.documentClickListener = this.renderer.listen('document', 'click', (event) => {
          this.hideModalEvent(event);
      });
    }
  }

  unbindDocumentClickListener() {
    if (this.documentClickListener) {
      this.documentClickListener();
      this.documentClickListener = null;
    }
  }

  private hideModalEvent(event) {
    if (this.element.nativeElement.contains(event.target) && event.target.contains(document.querySelector('.modal'))) {
      this.unbindDocumentClickListener();
      this.close();
    }
  }
}
