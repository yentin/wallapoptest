import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMoreItemsComponent } from './view-more-items.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';


const translations: any = { 'TEST': 'This is a test' };
class FakeLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return of(translations);
  }
}


describe('ViewMoreItemsComponent', () => {
  let component: ViewMoreItemsComponent;
  let fixture: ComponentFixture<ViewMoreItemsComponent>;
  const oldResetTestingModule = TestBed.resetTestingModule;

  beforeAll((done) => (async () => {
    TestBed.resetTestingModule();
    TestBed.configureTestingModule({
      declarations: [
        ViewMoreItemsComponent
      ],
      imports: [
        TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useClass: FakeLoader
        }
      })
    ]
    });
    await TestBed.compileComponents();

    // prevent Angular from resetting testing module
    TestBed.resetTestingModule = () => TestBed;

  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMoreItemsComponent);
    component = fixture.componentInstance;
    component.itemsShowing = 5;
    component.totalItems = 20;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init values fine with itemsShowing smaller than totalItems', () => {
    component.itemsShowing = 5;
    component.totalItems = 20;
    component.ngOnChanges();

    expect(component.percentage).toBe(25);
    expect(component.showMoreNumber).toBe(component.itemsShowing);

  });

  it('should init values fine with itemsShowing equal than totalItems', () => {
    component.itemsShowing = 14;
    component.totalItems = 14;
    component.ngOnChanges();

    expect(component.percentage).toBe(100);
    expect(component.showMoreNumber).toBe(component.totalItems - component.itemsShowing);

  });

  it('should init values fine with itemsShowing bigger than totalItems and showPerpage', () => {
    component.itemsShowing = 6;
    component.totalItems = 10;
    component.ngOnChanges();

    expect(component.percentage).toBe(60);
    expect(component.showMoreLinkShow ).toBeTruthy();

  });

  it('should init values fine with itemsShowing lower than totalItems', () => {
    component.itemsShowing = 21;
    component.totalItems = 20;
    component.ngOnChanges();

    expect(component.percentage).toBe(105);
    expect(component.showMoreLinkShow ).toBeFalsy();

  });

  it('should call nextPage', () => {
    const emitter = spyOn(component.onNextPage, 'emit');
    component.nextPage();

    expect(emitter).toHaveBeenCalled();
  });

  afterAll( () => {
    localStorage.clear();
    sessionStorage.clear();
    // reinstate resetTestingModule method
    TestBed.resetTestingModule = oldResetTestingModule;
    TestBed.resetTestingModule();
  });
});
