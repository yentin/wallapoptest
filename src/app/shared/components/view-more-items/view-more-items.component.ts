import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'view-more-items',
  templateUrl: './view-more-items.component.html',
  styleUrls: ['./view-more-items.component.scss']
})
export class ViewMoreItemsComponent implements OnInit, OnChanges {

  @Input() itemsShowing: number;
  @Input() totalItems: number;
  @Input() showPerPage = 5;
  @Output() onNextPage: EventEmitter<any> = new EventEmitter<any>();

  percentage: number;
  showMoreNumber: number;
  showMoreLinkShow = true;

  constructor(private translateService: TranslateService) { }

  ngOnInit() {
    this.percentage = Math.round((+this.itemsShowing) * 100 / (+this.totalItems));
    let quantityRest = this.totalItems - this.itemsShowing;
    if (quantityRest < 0) {
      quantityRest = 0;
    }
    if (quantityRest < this.showPerPage) {
      this.showMoreNumber = quantityRest;
      if (quantityRest === 0) {
        this.showMoreLinkShow = false;
      }
    } else {
      this.showMoreNumber = this.showPerPage;
    }
  }

  nextPage() {
    this.onNextPage.emit('');
  }

  ngOnChanges() {
    this.ngOnInit();
  }

}
