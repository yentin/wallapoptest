import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Renderer2, ElementRef } from '@angular/core';

import { CollapsibleComponent } from './collapsible.component';

export class MockElementRef extends ElementRef {
  constructor() { super(null); }
}

describe('CollapsibleComponent', () => {
  let component: CollapsibleComponent;
  let fixture: ComponentFixture<CollapsibleComponent>;
  const oldResetTestingModule = TestBed.resetTestingModule;

  beforeAll((done) => (async () => {
    TestBed.resetTestingModule();
    TestBed.configureTestingModule({
      declarations: [
        CollapsibleComponent
      ],
      providers: [
        Renderer2,
        { provide: ElementRef, useClass: MockElementRef }
      ]
    });

    await TestBed.compileComponents();

    // prevent Angular from resetting testing module
    TestBed.resetTestingModule = () => TestBed;

  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapsibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be selected item', () => {
    component.items = [
      {
        'id': 0,
        'title': 'Título ascencente'
      },
      {
        'id': 1,
        'title': 'Título descencente'
      }
    ];
    const selectedTestItem = component.items[0];
    expect(component.selectItem(0)).toBe(selectedTestItem);
  });

  it('should be classBig true size === big on ngOnInit', () => {
    component.size = 'big';
    component.ngOnInit();
    expect(component.classBig).toBeTruthy();
  });

  it('should be bindDocumentClickListener', () => {
    component.bindDocumentClickListener();
    expect(component.openCollapsible).toBeFalsy();
  });

  it('should be openCloseCollapsibleFunction', () => {
    component.openCollapsible = true;
    component.disabled = false;
    component.openCloseCollapsibleFunction();
    expect(component.openCollapsible).toBeFalsy();
  });

  afterAll( () => {
    localStorage.clear();
    sessionStorage.clear();
    // reinstate resetTestingModule method
    TestBed.resetTestingModule = oldResetTestingModule;
    TestBed.resetTestingModule();
  });

});
