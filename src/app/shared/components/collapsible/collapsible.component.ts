import { Component, OnInit, Input, EventEmitter, Output, OnDestroy, Renderer2, ElementRef } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'collapsible',
  templateUrl: './collapsible.component.html',
  styleUrls: ['./collapsible.component.scss']
})
export class CollapsibleComponent implements OnInit, OnDestroy {

  @Input() title: string;
  @Input() text: string;
  @Input() items: Array<any>;
  @Input() size = 'md';
  @Input() disabled = false;
  @Output() onSelected: EventEmitter<any> = new EventEmitter<any>();

  openCollapsible: boolean;
  documentClickListener: any;
  classBig: boolean;

  constructor(
    private renderer: Renderer2,
    private element: ElementRef) {
  }

  ngOnInit() {
    this.classBig = false;
    if (this.size === 'big') {
      this.classBig = true;
    }
    if (!this.disabled) {
      this.openCollapsible = false;
    }
  }

  selectItem(id) {
    const selectedObject = _.find(this.items, { 'id': id });
    this.text = selectedObject.title;
    this.onSelected.emit(selectedObject);
    this.openCollapsible = !this.openCollapsible;
    this.unbindDocumentClickListener();
    return selectedObject;

  }

  openCloseCollapsibleFunction() {
    if (!this.disabled) {
      this.openCollapsible = !this.openCollapsible;
      if (this.openCollapsible) {
        this.bindDocumentClickListener();
      } else {
        this.unbindDocumentClickListener();
      }
    }
  }

  bindDocumentClickListener() {
    if (!this.documentClickListener) {
      this.documentClickListener = this.renderer.listen('document', 'click', (event) => {
          this.hideCollapsibleEvent(event);
        });
    }
  }

  unbindDocumentClickListener() {
    if (this.documentClickListener) {
      this.documentClickListener();
      this.documentClickListener = null;
    }
  }

  private hideCollapsibleEvent(event) {
    if (!this.element.nativeElement.contains(event.target)) {
      this.openCollapsible = false;
      this.unbindDocumentClickListener();
    }
  }

  ngOnDestroy() {
    this.unbindDocumentClickListener();
  }
}
