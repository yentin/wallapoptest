import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'tag-keyword-result',
  templateUrl: './tag-keyword-result.component.html',
  styleUrls: ['./tag-keyword-result.component.scss']
})
export class TagKeywordResultComponent implements OnInit {

  @Input() text: any;
  @Input() class: string;
  @Output() delete: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {

  }

  clearSearch() {
    this.delete.emit();
  }
}
