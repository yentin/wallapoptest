import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { TagKeywordResultComponent } from '../tag-keyword-result/tag-keyword-result.component';
import { FormsModule } from '@angular/forms';


const translations: any = { 'TEST': 'This is a test' };
class FakeLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return of(translations);
  }
}


describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  const oldResetTestingModule = TestBed.resetTestingModule;

  beforeAll((done) => (async () => {
    TestBed.resetTestingModule();
    TestBed.configureTestingModule({
      declarations: [
        SearchComponent,
        TagKeywordResultComponent
      ],
      imports: [
        FormsModule,
        TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useClass: FakeLoader
        }
      })
    ]
    });
    await TestBed.compileComponents();

    // prevent Angular from resetting testing module
    TestBed.resetTestingModule = () => TestBed;

  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should call onEnter', () => {
    component.searchKeyword = 'mock';
    const emitter = spyOn(component.searchValue, 'emit');
    component.onEnter(component.searchKeyword);

    expect(component.keyword).toEqual(component.searchKeyword);
    expect(emitter).toHaveBeenCalled();
  });

  it('should keydown is an Enter', () => {
    const event = {keyCode: 13};
    const emitter = spyOn(component.searchValue, 'emit');
    component.keyDownFunction(event);
    expect(emitter).toHaveBeenCalled();
  });

  it('should keydown is not an Enter', () => {
    const event = {keyCode: 12};
    const emitter = spyOn(component.searchValue, 'emit');
    component.keyDownFunction(event);
    expect(emitter).not.toHaveBeenCalled();
  });

  it('should clean search', () => {
    component.searchKeyword = 'mock';
    const emitter = spyOn(component.searchValue, 'emit');
    component.onDeleteSearchTag();
    expect(component.searchKeyword).toEqual('');
    expect(emitter).toHaveBeenCalled();
  });

  it('should write model when select checkbox and check model controls functions', () => {
    const value = 'mock2';
    component.searchKeyword = 'mock';
    component.writeValue(value);

    expect (component.searchKeyword).toEqual(value);

    component.registerOnChange(() => { });
    component.registerOnTouched(() => { });
  });

  it('should write model same value when select checkbox and check model controls functions', () => {
    const value = 'mock';
    component.searchKeyword = 'mock';
    component.writeValue(value);

    expect (component.searchKeyword).toEqual(value);

    component.registerOnChange(() => { });
    component.registerOnTouched(() => { });
  });

  afterAll( () => {
    localStorage.clear();
    sessionStorage.clear();
    // reinstate resetTestingModule method
    TestBed.resetTestingModule = oldResetTestingModule;
    TestBed.resetTestingModule();
  });
});
