import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Output() searchValue: EventEmitter<any> = new EventEmitter();
  searchKeyword = '';
  keyword = '';


  private onTouchedCallback: () => void = () => { };
  private onChangeCallback: (_: any) => void = () => { };

  constructor() { }

  ngOnInit() {
  }

  onEnter(value: string) {
    this.keyword = this.searchKeyword;
    this.searchValue.emit(value);
  }

  keyDownFunction(event) {
    if (event.keyCode === 13) {
      this.onEnter(this.searchKeyword);
    }
  }

  onDeleteSearchTag() {
    this.searchKeyword = '';
    this.onEnter(this.searchKeyword);
  }

  writeValue(value: any) {
    if (value !== this.searchKeyword) {
      this.searchKeyword = value;
    }
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

}
