import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { environment } from 'src/environments/environment';
import { SearchComponent } from './components/search/search.component';
import { TagKeywordResultComponent } from './components/tag-keyword-result/tag-keyword-result.component';
import { ViewMoreItemsComponent } from './components/view-more-items/view-more-items.component';
import { ModalComponent } from './components/modal/modal.component';
import { ModalHeaderComponent } from './components/modal/modal-header/modal-header.component';
import { ModalBodyComponent } from './components/modal/modal-body/modal-body.component';
import { ModalFooterComponent } from './components/modal/modal-footer/modal-footer.component';
import { CollapsibleComponent } from './components/collapsible/collapsible.component';



@NgModule({
  declarations: [SearchComponent, TagKeywordResultComponent, ViewMoreItemsComponent,
    ModalComponent, ModalHeaderComponent, ModalBodyComponent, ModalFooterComponent, 
    CollapsibleComponent],
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http, './assets/i18n/', `.json?ver=${environment.version}`);
        },
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    SearchComponent,
    TagKeywordResultComponent,
    ViewMoreItemsComponent,
    ModalComponent,
    ModalHeaderComponent,
    ModalFooterComponent,
    ModalBodyComponent,
    CollapsibleComponent,
    TranslateModule
  ]
})
export class SharedModule { }
