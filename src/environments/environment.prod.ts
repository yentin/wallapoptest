export const environment = {
  production: true,
  local: false,
  version: '0.0.1',
  BASE_URL: 'http://localhost:4200'
};
